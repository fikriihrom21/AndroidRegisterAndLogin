package com.example.androidregisterdanlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText email,password;
    private Button loginBtn;
    private ProgressBar loading;
    private TextView linkReg;
    private static String URL_LOGIN ="http://192.168.0.109/android_register_login/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.textEmailAddress);
        password = (EditText) findViewById(R.id.textPassword);
        loginBtn = (Button) findViewById(R.id.loginButton);
        loading = (ProgressBar) findViewById(R.id.loading2);
        linkReg = (TextView) findViewById(R.id.linkReg);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mEmail = email.getText().toString().trim();
                String mPassword = password.getText().toString().trim();

                if (!mEmail.isEmpty() || !mPassword.isEmpty()){

                    login();

                }else {
                    email.setError("Please Insert email ");
                    password.setError("Please Insert password");
                }

            }
        });

    }

    private void login() {
        loading.setVisibility(View.VISIBLE);
        loginBtn.setVisibility(View.GONE);

        final  String email = this.email.getText().toString();
        final  String password = this.password.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String success = jsonObject.getString("success");
                    JSONArray js = jsonObject.getJSONArray("login");
                    if (success.equals("1")){
                        for (int i = 0; i<js.length(); i++){
                            JSONObject object = js.getJSONObject(i);
                            String name = object.getString("name").trim();
                            String email = object.getString("email").trim();
                            Toast.makeText(LoginActivity.this, "Login berhasil !. \n Your Name :"+name+"email : "+email, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            loginBtn.setVisibility(View.VISIBLE);
                        }
                    }

                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Login Error ! "+ e.toString(), Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Login Error ! "+ error.toString(), Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                        loginBtn.setVisibility(View.VISIBLE);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("email",email);
                params.put("password",password);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}